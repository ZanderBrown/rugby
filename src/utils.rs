use std::time::Duration;

use matrix_sdk::{
    room::Invited,
    ruma::events::{
        room::message::{EmoteMessageEventContent, MessageType, RoomMessageEventContent},
        SyncMessageEvent,
    },
    BaseRoomMember, Result,
};
use tokio::time::sleep;
use tracing::warn;

/// A simplified way of getting the text from a message event
pub fn get_message_event_text(event: &SyncMessageEvent<RoomMessageEventContent>) -> Option<String> {
    if let SyncMessageEvent {
        content:
            RoomMessageEventContent {
                msgtype: MessageType::Emote(EmoteMessageEventContent { body: msg_body, .. }),
                ..
            },
        ..
    } = event
    {
        return Some(msg_body.to_owned());
    }
    None
}

/// Gets display name for a RoomMember
/// falls back to user_id for accounts without displayname
pub fn get_member_display_name(member: &BaseRoomMember) -> String {
    member
        .display_name()
        .unwrap_or_else(|| member.user_id().as_str())
        .to_string()
}

#[async_trait::async_trait]
pub trait BackoffAccept {
    async fn forgiving_accept_invite(&self, max_delay: u64) -> Result<()>;
}

#[async_trait::async_trait]
impl BackoffAccept for Invited {
    async fn forgiving_accept_invite(&self, max_delay: u64) -> Result<()> {
        let mut delay = 1;

        while let Err(err) = self.accept_invitation().await {
            warn!(
                "Failed to join room {} ({:?}), retrying in {}s",
                self.room_id(),
                err,
                delay
            );

            sleep(Duration::from_secs(delay)).await;
            delay *= 2;

            if delay > max_delay {
                return Err(err);
            }
        }

        Ok(())
    }
}
