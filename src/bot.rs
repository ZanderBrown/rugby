use chrono::prelude::*;
use chrono_tz::OffsetName;
use itertools::Itertools;
use matrix_sdk::{
    config::SyncSettings,
    event_handler::Ctx,
    room::Room,
    ruma::{
        events::{
            room::message::SyncRoomMessageEvent,
            room::{member::StrippedRoomMemberEvent, message::RoomMessageEventContent},
            AnyMessageEventContent,
        },
        UserId,
    },
    uuid::Uuid,
    BaseRoomMember, Client, RoomMember,
};
use rand::prelude::*;
use sd_notify::{notify, NotifyState};
use sqlx::PgPool;
use std::env;
use tracing::info;

use crate::{
    config::Zone,
    utils::{get_member_display_name, get_message_event_text, BackoffAccept},
    Config,
};

#[derive(Clone)]
pub struct Bot {
    client: Client,
    db_pool: PgPool,
}

impl Bot {
    pub async fn run() {
        let config = Config::read();

        let username = config.bot_user_id.as_str();
        let user = Box::<UserId>::try_from(username).expect("Unable to parse bot user id");
        let client = Client::new_from_user_id(&user).await.unwrap();

        Self::login(&client, user.localpart(), &config.bot_password).await;

        let db = env::var("DATABASE_URL").expect("Missing DATABASE_URL");
        let db_pool = PgPool::connect(&db)
            .await
            .expect("Failed to connect to database");

        sqlx::migrate!()
            .run(&db_pool)
            .await
            .expect("Failed to run migrations");

        let bot = Self { client, db_pool };

        // Setup event handler
        bot.client
            .register_event_handler_context(bot.clone())
            .register_event_handler(Self::on_room_message)
            .await
            .register_event_handler(Self::on_stripped_state_member)
            .await;

        info!("Start syncing...");
        bot.client.sync(SyncSettings::new()).await;
    }

    /// Login
    async fn login(client: &Client, user: &str, pwd: &str) {
        info!("Logging in...");
        let response = client
            .login(user, pwd, Some("rugby"), Some("rugby"))
            .await
            .expect("Unable to login");

        info!("Do initial sync...");
        client
            .sync_once(SyncSettings::new())
            .await
            .expect("Unable to sync");

        info!(
            "Logged in as {}, got device_id {}",
            response.user_id, response.device_id
        );

        notify(false, &[NotifyState::Ready]).expect("Can't signal to systemd");

        for room in client.invited_rooms() {
            room.forgiving_accept_invite(3600)
                .await
                .expect("Unable to accept invite");
            info!("Successfully joined room {}", room.room_id());
        }
    }

    /// Handling room messages events
    async fn on_room_message(event: SyncRoomMessageEvent, room: Room, Ctx(bot): Ctx<Bot>) {
        if let Some(text) = get_message_event_text(&event) {
            match text.as_str().trim() {
                "wonders when people are" => bot.members_time(room).await,
                "wonders when they are" => {
                    let member = room.get_member(&event.sender).await.unwrap().unwrap();
                    bot.member_time(room, member).await
                }
                _ => (),
            }
        }
    }

    async fn on_stripped_state_member(
        room_member: StrippedRoomMemberEvent,
        client: Client,
        room: Room,
        Ctx(_bot): Ctx<Bot>,
    ) {
        if room_member.state_key != client.user_id().await.unwrap() {
            return;
        }

        if let Room::Invited(room) = room {
            room.forgiving_accept_invite(3600)
                .await
                .expect("Unable to accept invite");

            info!("Successfully joined room {}", room.room_id());
        }
    }

    async fn format_member_zones(
        &self,
        now: DateTime<Utc>,
        members: Vec<RoomMember>,
    ) -> Option<String> {
        let now_naive = now.naive_utc();

        let mut zones = Vec::new();
        for member in members {
            if let Some(zone) = self
                .zone_for_member(&member)
                .await
                .expect("this is bad bad")
            {
                zones.push(zone);
            }
        }

        if zones.is_empty() {
            None
        } else {
            Some(
                zones
                    .iter()
                    .sorted_by_key(|zone| zone.tz.name())
                    .dedup()
                    .map(|zone| (zone.tz.offset_from_utc_datetime(&now_naive), &zone.badge))
                    .into_group_map_by(|(offset, _)| offset.fix())
                    .into_iter()
                    .map(|(offset, meta)| {
                        (
                            offset,
                            meta.into_iter()
                                .map(|(tz, badge)| (tz.abbreviation().to_owned(), badge))
                                .collect::<Vec<_>>(),
                        )
                    })
                    .sorted_by_key(|(offset, _)| offset.local_minus_utc())
                    .rev()
                    .map(|(offset, mut meta)| {
                        let mut rng = thread_rng();
                        let meta = meta.as_mut_slice();
                        meta.shuffle(&mut rng);
                        (now.with_timezone(&offset), Vec::from(meta))
                    })
                    .map(|(local, meta)| {
                        let (first_name, _) = meta.first().expect("really");
                        format!(
                            "<code>{:>4}: {}{} {}</code> ({})",
                            first_name,
                            local.day(),
                            match local.day() {
                                1 | 21 | 31 => "st",
                                2 | 22 => "nd",
                                3 | 23 => "rd",
                                _ => "th",
                            },
                            local.format("%H∶%M∶%S"),
                            meta.iter()
                                .map(|(name, badge)| if name == first_name {
                                    badge.to_string()
                                } else {
                                    format!("{} [{}]", badge, name)
                                })
                                .join(", ")
                        )
                    })
                    .join("<br>"),
            )
        }
    }

    async fn members_time(&self, room: Room) {
        let now = Utc::now();
        let txn_id = Uuid::new_v4();

        if let Room::Joined(ref joined) = room {
            if let Ok(members) = room.joined_members().await {
                joined
                    .send(
                        AnyMessageEventContent::RoomMessage(RoomMessageEventContent::notice_html(
                            "blah",
                            if let Some(content) = self.format_member_zones(now, members).await {
                                content
                            } else {
                                "I spy strangers!".to_string()
                            },
                        )),
                        Some(txn_id),
                    )
                    .await
                    .expect("Unable to send message");
            }
        }
    }

    async fn member_time(&self, room: Room, member: RoomMember) {
        let now = Utc::now();
        let txn_id = Uuid::new_v4();

        if let Room::Joined(ref joined) = room {
            joined
                .send(
                    AnyMessageEventContent::RoomMessage(RoomMessageEventContent::notice_html(
                        "blah",
                        format!(
                            "{} you are<br>{}",
                            get_member_display_name(&member),
                            if let Some(content) = self.format_member_zones(now, vec![member]).await {
                                content
                            } else {
                                "A mystery".to_string()
                            }
                        ),
                    )),
                    Some(txn_id),
                )
                .await
                .expect("Unable to send message");
        }
    }

    async fn zone_for_member(&self, member: &BaseRoomMember) -> Result<Option<Zone>, sqlx::Error> {
        let username = member.user_id().as_str().to_string();
        sqlx::query!(
            "
    SELECT z.tzid, badge
    FROM rugby_zone z
    JOIN rugby_person p ON p.tzid = z.tzid
    WHERE mxid = $1
            ",
            username
        )
        .fetch_optional(&self.db_pool) // -> Vec<{ country: String, count: i64 }>
        .await
        .map(|z| {
            z.map(|z| Zone {
                tz: z.tzid.parse().expect("blah"),
                badge: z.badge,
            })
        })
    }
}
