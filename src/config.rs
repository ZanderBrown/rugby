use chrono_tz::Tz;
use serde::{Deserialize, Serialize};

use std::{env, fs::File, io::Read};

#[derive(Debug, PartialEq)]
pub struct Zone {
    pub tz: Tz,
    pub badge: String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Config {
    pub bot_user_id: String,
    pub bot_password: String,
}

impl Config {
    pub fn read() -> Self {
        let path = match env::var("CONFIG_PATH") {
            Ok(val) => val,
            Err(_) => "./config.json".to_string(),
        };

        let mut file = File::open(path).expect("Unable to open configuration file");
        let mut data = String::new();
        file.read_to_string(&mut data)
            .expect("Unable to read configuration file");

        serde_json::from_str(&data).expect("Unable to parse configuration file")
    }
}
