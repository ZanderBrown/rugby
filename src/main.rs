mod bot;
mod config;
mod utils;

use std::env;

use tracing::{debug, Level};
use tracing_subscriber::{filter, prelude::*};

pub use config::Config;

#[tokio::main(flavor = "current_thread")]
async fn main() {
    dotenv::dotenv().ok();

    let production = env::var("MODE").map(|v| v == "prod").unwrap_or(false);

    let tracing = tracing_subscriber::registry().with(filter::filter_fn(|metadata| {
        metadata.target() == env!("CARGO_PKG_NAME") || *metadata.level() < Level::WARN
    }));
    if production {
        tracing
            .with(
                tracing_journald::layer()
                    .expect("Unable to load journald layer")
                    .with_field_prefix(Some("RUGBY".to_string())),
            )
            .init();
    } else {
        tracing.with(tracing_subscriber::fmt::layer()).init();
    }

    debug!("Starting rugby");

    bot::Bot::run().await;
}
