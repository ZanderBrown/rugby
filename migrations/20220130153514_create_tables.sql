CREATE TABLE IF NOT EXISTS rugby_zone (
    tzid   VARCHAR(29) PRIMARY KEY,
    badge  VARCHAR(10) NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS rugby_person (
    mxid   VARCHAR(255) PRIMARY KEY,
    tzid   VARCHAR(29) NOT NULL,
    
    FOREIGN KEY(tzid) REFERENCES rugby_zone(tzid)
);
